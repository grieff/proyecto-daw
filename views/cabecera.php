<!-- Navbar--> 

<!-- Lista links
  - Inicio (seleccionado por defecto)
  - Historia
    - Origenes
    - Directores
  - Galeria
    - Banda Juvenil
    - Banda Municipal
  - Eventos
  - Conócenos
    - Plantilla
    - Junta Directiva
  - Contacto

  - Iniciar Sesion
   -->



<nav class="navbar navbar-dark  indigo darken-2">
  <!-- Navbar brand -->
  <a class="navbar-brand" href="#">Banda Municipal </a>
  <span style="color: white">
  <?php if(isset($_SESSION['username'])) { 

    echo $_SESSION['username'];


  }?>
  </span>
  
  
  <!-- Collapse button -->
  <button class="navbar-toggler third-button" type="button" data-toggle="collapse" data-target="#navbarSupportedContent22"
    aria-controls="navbarSupportedContent22" aria-expanded="false" aria-label="Toggle navigation">
    <div class="animated-icon3"><span></span><span></span><span></span></div>
  </button>

  <!-- Collapsible content -->
  <div class="collapse navbar-collapse" id="navbarSupportedContent22">

    <!-- Links -->
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="index.php?op=0">Inicio</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="index.php?op=1">Historia</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="index.php?op=2">Galeria</a>
      </li>
      <!-- Si esta iniciada la sesion, muestra los formularios -->
      <?php if(isset($_SESSION['username'])) { ?>
      
      <li class="nav-item">
        <a class="nav-link" href="index.php?op=3">Eventos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="index.php?op=4">Conócenos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="logout.php">Contacto</a>
      </li>
    <?php } else { ?>
      <!-- Si no esta iniciada la sesion, entonces se puede iniciar sesion -->
      <li class="nav-item">
        <a class="nav-link" href="index.php?op=5">Iniciar sesion</a>
      </li> 
    <?php } ?>
   
    </ul>
    <!-- Links -->

  </div>
  <!-- Collapsible content -->

</nav>
<!--/.Navbar
<?php 
	include "bbdd/conexion.php";

	session_start();
	if(!isset($_SESSION['username'])) {
		$_SESSION['username'] ="";
	} else {
		$usuario = $_SESSION['username'];
		
	}
	

	

	

  ?>



<!DOCTYPE html>
<html>
<head>

	<title>Banda Municipal Sto Cristo de los Milagros</title>
	<!-- Hojas de estilo -->
		<!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="css/mdb.min.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="css/style.css" rel="stylesheet">
  <!-- FullCalendar.io CSS -->
  <link href='fullcalendar/core/main.css' rel='stylesheet' />
  <link href='fullcalendar/daygrid/main.css' rel='stylesheet' />

    

</head>
<body>
	<!-- Cabecera -->
	<?php include "navegacion.php";
	?>

	<div id="index">


		<!-- Mostrar cuerpo distinto segun se eliga en el menu -->
		
		<?php 
		 if (isset($_GET['op'])) {
			switch ($_GET['op']) {
				case '0':
					include "cuerpo.php"; // Cuerpo de inicio
					break;
				case '1':
					include "list_usuarios.php";
					break;
				case '2':
					include "list_noticias.php";
					break;
				case '3':
					include "form/form_usuarios.php";
					break;
				case '4':
					include "form/form_noticias.php";
					break;	
				case '5':
					include "login.php";
					break;	
				case '6':
					include "form/mod_usuarios.php";
					break;
				case '7':
					include "form/mod_noticias.php";
					break;
				
			};
		} else {
			// Si la sesion esta iniciada, incluye cuerpo
			if(isset($_SESSION['username'])) {
				include "cuerpo.php";
			} else {
				// Si no hay una sesion, entonces se muestra el login
				include "login.php";
			}


			
			
		};
 		?> 



	</div>


	<!-- Pie -->
		<!-- Incluye scripts -->
	<?php  //include "pie.php" ?>

	
    <!-- Scripts -->
	<script type="text/javascript" src="js/jquery-3.5.0.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="js/mdb.min.js"></script>
  <!-- FullCalendar.io JavaScript -->
  <script src='fullcalendar/core/main.js'></script>
  <script src='fullcalendar/daygrid/main.js'></script>
	
	
</body>
</html>